package com.epam;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class TestArray {
    MenuItem menuItem;
    MenuItem menuItem2;

    @Before
    public void createClassObject() {
        menuItem = new MenuItem("1", "FillAray");
    }

    @Test
    public void checkOrNameTrue() {
        assertEquals(menuItem.getName(), "FillAray");
    }

    @Test
    public void checkOrIdTrue() {
        assertEquals(menuItem.getId(), "1");
    }

    @Test
    public void checkObject() {
        assertNotNull(menuItem);

    }

    @Test
    public void checkIsNull() {
        assertNull(menuItem2);

    }

    @Test
    public void equalTwoObject() {
        assertFalse(menuItem.equals(menuItem2));
    }

}
