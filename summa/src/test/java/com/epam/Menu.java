package com.epam;

public class Menu {

    Array arrayClass;
    int[] arrays;
    static int clics;
    private boolean status = true;

    MenuItem randomFill = new MenuItem("1", "Random fill");
    MenuItem showArray = new MenuItem("2", "Show Array");
    MenuItem avarageNumber = new MenuItem("3", "Show avarage number");
    MenuItem showEvenNumber = new MenuItem("4", "Show even number");
    MenuItem sortArray = new MenuItem("5", "Sort Array");
    MenuItem exit = new MenuItem("q", "For exit");
    MenuItem[] items = { randomFill, showArray, avarageNumber, showEvenNumber,
            sortArray, exit };

    public Menu(int[] array) {
        this.arrays = array;
        arrayClass = new Array(arrays);
        clics = 0;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public void banner() {

        arrayClass.messagePrintln("\nPlease select function:\n");

    }

    public void display() {

        banner();
        for (int i = 0; i < items.length; i++) {

            arrayClass
                    .messagePrintln(items[i].getId() + items[i].displayName());
        }
    }

    public boolean isActive() {
        return status;
    }

    public void viewArray(int array[]) {
        arrayClass.messagePrint("\n[");

        for (int arrayIn : array) {
            arrayClass.messagePrint(arrayIn + " ");

        }
        arrayClass.messagePrint("]\n");

    }

    public boolean isChoseFill() {
        if (clics < 1) {
            arrayClass.messagePrintln("Please, fill Array\n");

            return true;
        } else {
            return false;
        }
    }

    public void itemForFillArray(String Item) {
        if (Item.equals(randomFill.getId())) {
            clics++;
            arrayClass.fillArray();

            arrayClass.messagePrintln("\tARRAY: ");
            viewArray(arrays);
        }

    }

    public void itemForViewArray(String Item) {
        if (Item.equals(showArray.getId())) {

            if (isChoseFill()) {

            } else {
                arrayClass.messagePrintln("\tARRAY: ");
                viewArray(arrays);

            }
        }

    }

    public void itemForShowAvarage(String Item) {
        if (Item.equals(avarageNumber.getId())) {
            if (isChoseFill()) {

            } else {
                double avarage = 0;
                avarage = arrayClass.getAvarage();
                viewArray(arrays);
                arrayClass.messagePrintln("\nAvarage from is " + avarage);

            }

        }
    }

    public void itemForViewEvenArray(String Item) {
        if (Item.equals(showEvenNumber.getId())) {
            if (isChoseFill()) {

            } else {

                int[] even_array = arrayClass.getEvenArray();
                viewArray(even_array);
            }
        }
    }

    public void itemForSorted(String Item) {
        if (Item.equals(sortArray.getId())) {

            if (isChoseFill()) {

            } else {
                arrayClass.sortedArray();
                arrayClass.messagePrint("Sorted Array");
                viewArray(arrays);
            }
        }
    }

    public void exit(String Item) {
        if (Item.equals("q") || Item.equals("Q")) {
            setStatus(false);
            arrayClass.messagePrintln("\tGood bye! ");

        }

    }

    public boolean checkOrWordIsItemOnMenu(String word) {
        for (int i = 0; i < items.length; i++) {
            if (items[i].getId().equals(word)) {

                return true;
            } else {

            }
        }
        return false;
    }

    public void selectMenuItem(String Item) {
        if (checkOrWordIsItemOnMenu(Item)) {
            itemForFillArray(Item);
            itemForViewArray(Item);
            itemForShowAvarage(Item);
            itemForViewEvenArray(Item);
            itemForSorted(Item);
            exit(Item);
        } else {
            arrayClass.messagePrintln("You enter the wrong symbol !\n");
        }
    }
}

