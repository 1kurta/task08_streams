package com.epam;

public class MenuItem {
    private String id;
    private String name;

    public MenuItem(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }
    public String displayName() {
        return String.format(" : %s",getName());

    }
}

