package com.epam;

import java.util.Arrays;
import java.util.Random;

public class Array {
    int array[];

    int oddNumberArray[];
    int arrayEvenNumbers[];
    public int evenNumber;

    public Array(int arr[]) {

        this.array = arr;

    }

    public void messagePrintln(String s) {
        System.out.println(s);
    }

    public void messagePrint(String s) {

        System.out.print(s);
    }

    public void computeNumber() {

        for (int i = 0; i < array.length; i++) {
            if (array[i] % 2 == 0) {
                evenNumber++;
            }

        }

    }

    public boolean condition(int n) {

        if (n % 2 == 0) {
            return true;
        }
        return false;
    }

    public int[] getEvenArray() {
        int amount = 0;
        for (int i = 0; i < array.length; i++) {
            if (condition(array[i])) {
                amount++;
            }
        }
        int arrayPosition = 0;
        arrayEvenNumbers = new int[amount];
        for (int i = 0; i < array.length; i++) {
            if (condition(array[i])) {
                arrayEvenNumbers[arrayPosition] = array[i];
                arrayPosition++;
            }
        }
        return arrayEvenNumbers;

    }

    public double getAvarage() {
        double avarage = 0;
        for (int i = 0; i < array.length; i++) {
            avarage += array[i];
        }
        avarage /= array.length;

        return avarage;
    }

    public int[] sortedArray() {
        Arrays.sort(array);

        return array;
    }

    public void fillArray() {
        Random rand = new Random();
        for (int i = 0; i < array.length; i++) {
            array[i] = rand.nextInt(10);
        }

    }

}
