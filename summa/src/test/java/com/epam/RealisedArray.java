package com.epam;

import java.util.Scanner;

public class RealisedArray {

    public static void main(String[] args) {

        int[] array = new int[10];
        Menu menu = new Menu(array);

        String menuItem;

        while (menu.isActive()) {
            Scanner scanner = new Scanner(System.in);
            menu.display();
            menuItem = scanner.nextLine();
            menu.selectMenuItem(menuItem);

        }

    }
}
